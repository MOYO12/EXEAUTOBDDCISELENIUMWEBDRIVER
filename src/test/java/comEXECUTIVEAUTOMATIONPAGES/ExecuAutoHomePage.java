package comEXECUTIVEAUTOMATIONPAGES;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by User on 19/04/2017.
 */
public class ExecuAutoHomePage {

    public WebDriver driver;

             public ExecuAutoHomePage(WebDriver driver){
                 this.driver = driver;
                 PageFactory.initElements(driver,this);

             }
             @FindBy(how = How.NAME, using = "UserName" )public static WebElement UserN;
             @FindBy(how = How.NAME,using = "Password")public static WebElement Pword;
             @FindBy(how = How.XPATH,using = "//*[@id=\'userName\']/p[3]/input")public static WebElement Login;



             public void Access(String Uname,String Pwd){
                 UserN.sendKeys(Uname);
                 Pword.sendKeys(Pwd);
                 Login.click();
             }
}
