package comEXECUTIVEAUTOMATIONPIPE;

import comEXECUTIVEAUTOMATIONBASE.BaseExecAuto;
import comEXECUTIVEAUTOMATIONPAGES.ExecuAutoHomePage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;

/**
 * Created by User on 19/04/2017.
 */
public class ExecAutoStepDefin extends BaseExecAuto {
    public WebDriver driver;

           public ExecAutoStepDefin(){
               this.driver = ExecAutoHOOK.driver;
           }


       @Given("^That am on the home page$")
    public void that_am_on_the_home_page() throws Throwable {
         driver.get("http://executeautomation.com/demosite/Login.html");
    }

    @Then("^As a user am able to enter \"([^\"]*)\" and \"([^\"]*)\" login details$")
    public void as_a_user_am_able_to_enter_and_login_details(String Uname, String Pwd) throws Throwable {
        ExecuAutoHomePage EAH = new ExecuAutoHomePage(driver);
        EAH.Access(Uname, Pwd);
    }

    @When("^As a user am able to click login$")
    public void as_a_user_am_able_to_click_login() throws Throwable {
        ExecuAutoHomePage EAH = new ExecuAutoHomePage(driver);

    }

    @And("^As a user am able to see a user form$")
    public void as_a_user_am_able_to_see_a_user_form() throws Throwable {

    }


}
