package comEXECUTIVEAUTOMATIONPIPE;

/**
 * Created by User on 19/04/2017.
 */


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/ResourceBoot",
        plugin = {"pretty", "html:target/cucumber", "json:target/cucumber.json"},
       // format = {"json:target/cucumber.json"},
        tags = "@JUNIOR",
        monochrome = false


)

public class ExecAutoTestRunner {
}
